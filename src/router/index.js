import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/list/comitia/index',
    name: 'circle_list_comitia',
    component: () => import('../views/list/comitia/index.vue')
  },
  {
    path: '/list/comitia/130',
    name: 'comitia_130',
    component: () => import('../views/list/comitia/130.vue')
  },
  {
    path: '/list/comitia/131',
    name: 'comitia_131',
    component: () => import('../views/list/comitia/131.vue')
  },
  {
    path: '/list/comitia/134',
    name: 'comitia_134',
    component: () => import('../views/list/comitia/134.vue')
  },
  {
    path: '/list/comitia/136',
    name: 'comitia_136',
    component: () => import('../views/list/comitia/136.vue')
  },
  {
    path: '/list/comitia/137',
    name: 'comitia_137',
    component: () => import('../views/list/comitia/137.vue')
  },
  {
    path: '/list/comitia/138',
    name: 'comitia_138',
    component: () => import('../views/list/comitia/138.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
