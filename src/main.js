import Vue from 'vue'
import App from './App.vue'
import router from './router'
//option add
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'
import titleMixin from './util/title'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'

library.add(fas, far, fab)

Vue.use(VueAxios, axios)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.mixin(titleMixin)


Vue.config.productionTip = false

Vue.prototype.$axios = axios

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
