# Circle list searcher

[![Netlify Status](https://api.netlify.com/api/v1/badges/0065ded4-6103-4045-9ee9-3f7d721df806/deploy-status)](https://app.netlify.com/sites/circle-search/deployshttps://app.netlify.com/sites/cocky-jang-954beb/deploys)

## これなに

イベントのサークルリストを検索するやつ  
即売会で最後尾札作るのにサークルスペースとサークル名を毎回カタログで引くのが面倒なので作りました。

## How to Use

```
git pull --depth 1 git@gitlab.com:comitia-tools/circle-list-searcher.git
yarn install
yarn build
```

## Hosting

このサイトは[Netlify](https://www.netlify.com/)で公開されています。  

## License

WTFPL(Do What the Fuck You Want to Public License)
